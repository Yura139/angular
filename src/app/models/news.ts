export interface News {
    id: number;
    title: string;
    author: string;
    desc: string;
    img: string;
}

