export interface Blog {
  id: number;
  title: string;
  data: number;
  author: string;
  desc: string;
}