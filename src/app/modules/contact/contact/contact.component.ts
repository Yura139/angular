import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor() { }

  loginForm;

  ngOnInit(): void {
    this.createForm();
  }

  createForm(){
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      comment: new FormControl('', [Validators.required])
    });
  }

  onSubmit(){
    if(this.loginForm.invalid) return false;
    console.log(this.loginForm.value);
    console.log(this.loginForm);
  }


}
