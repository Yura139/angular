import { Component, OnInit, Input } from '@angular/core';
import { NewsService } from '../services/news.service';

@Component({
  selector: 'app-item-news',
  templateUrl: './item-news.component.html',
  styleUrls: ['./item-news.component.scss']
})
export class ItemNewsComponent implements OnInit {

  constructor(private newsServices: NewsService) { }
@Input() item;
  ngOnInit(): void {
  }

  thisClick(item){
    this.newsServices.setSelectedNews(item);
  }
}
