import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { NewsService } from '../services/news.service';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit, AfterViewInit,  OnDestroy{

  constructor(private newsService: NewsService) { }

  restList = [];

  ngOnInit(): void {
  // this.restList =  this.newsService.getNews();


  this.newsService.getData().subscribe(res => this.restList = res);

    }
    ngAfterViewInit() {
      console.log('View');
    }

    ngOnDestroy() {
      console.log('destroy');
    }

}
