import { Injectable } from '@angular/core';
import { News } from 'src/app/models/news';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor( private http: HttpClient) { }

//  private listNews: News[] = [
//     {
//       id: 1,
//       title: '5 ways to create inspiring articles',
//       author: 'by Joe Bloggs',
// tslint:disable-next-line: max-line-length
//       desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis molestie a iaculis at erat pellentesque adipiscing. Vulputate dignissim suspendisse in est ante in nibh. Purus non enim praesent elementum.',
//       img: 'https://source.unsplash.com/9XrYUO8laY4/800x600'
//     },
//     {
//       id: 2,
//       title: '5 ways to create inspiring articles',
//       author: 'by Joe Bloggs',
// tslint:disable-next-line: max-line-length
//       desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis molestie a iaculis at erat pellentesque adipiscing. Vulputate dignissim suspendisse in est ante in nibh. Purus non enim praesent elementum.',
//       img: 'https://source.unsplash.com/AeoslwNGA-8/800x600'
//     },
//     {
//       id: 3,
//       title: '5 ways to create inspiring articles',
//       author: 'by Joe Bloggs',
// tslint:disable-next-line: max-line-length
//       desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis molestie a iaculis at erat pellentesque adipiscing. Vulputate dignissim suspendisse in est ante in nibh. Purus non enim praesent elementum.',
//       img: 'https://source.unsplash.com/a0U58gHCVRw/800x600'
//     },
//     {
//       id: 4,
//       title: '5 ways to create inspiring articles',
//       author: 'by Joe Bloggs',
// tslint:disable-next-line: max-line-length
//       desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis molestie a iaculis at erat pellentesque adipiscing. Vulputate dignissim suspendisse in est ante in nibh. Purus non enim praesent elementum.',
//       img: 'https://source.unsplash.com/a0U58gHCVRw/800x600'
//     },
//     {
//       id: 5,
//       title: '5 ways to create inspiring articles',
//       author: 'by Joe Bloggs',
// tslint:disable-next-line: max-line-length
//       desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis molestie a iaculis at erat pellentesque adipiscing. Vulputate dignissim suspendisse in est ante in nibh. Purus non enim praesent elementum.',
//       img: 'https://source.unsplash.com/AeoslwNGA-8/800x600'
//     },
//     {
//       id: 6,
//       title: '5 ways to create inspiring articles',
//       author: 'by Joe Bloggs',
// tslint:disable-next-line: max-line-length
//       desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis molestie a iaculis at erat pellentesque adipiscing. Vulputate dignissim suspendisse in est ante in nibh. Purus non enim praesent elementum.',
//       img: 'https://source.unsplash.com/a0U58gHCVRw/800x600'
//     }
//   ];

 private selected: News;

  getSelectedNews(): News{
    return this.selected;
  }

  setSelectedNews(news: News){
    this.selected = news;
  }

  // getNews(): News[]{
  //   return this.listNews;
  // }

  // getNewsByID(id: number): News {
  //   this.selected = this.listNews.find(item => item.id === id);
  //   return this.selected;
  // }

  getData(): Observable<News[]>{
        return this.http.get<News[]>('assets/news.json');
  }

  getDataById(id): Observable<News>{
    return this.http.get<News>('assets/news.json').pipe(map((res: any) => {
      return res.find(item => item.id === id);
    }));
}
}

