import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NewsListComponent} from "./news-list/news-list.component";
import {NewCardComponent} from "./new-card/new-card.component"

const routes: Routes = [
 { path: "", component: NewsListComponent},
 { path: "card/:id", component: NewCardComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule { }
