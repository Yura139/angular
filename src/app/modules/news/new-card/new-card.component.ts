import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from '../services/news.service';
import { News } from 'src/app/models/news';

@Component({
  selector: 'app-new-card',
  templateUrl: './new-card.component.html',
  styleUrls: ['./new-card.component.scss'],
})
export class NewCardComponent implements OnInit {
  currentId: number;
  constructor(
    private route: ActivatedRoute,
    private newsServices: NewsService
  ) {
    //  this.currentId = route.snapshot.params.id;
    route.params.subscribe((params) => {
      this.currentId = +params.id;
      this.getData();
    });
  }
  currentItem: News;
  ngOnInit(): void {}

  getData() {
    if (
      this.newsServices.getSelectedNews() &&
      this.newsServices.getSelectedNews().id === this.currentId
    ) {
      this.currentItem = this.newsServices.getSelectedNews();
    }else{
      this.getItemsById(this.currentId);
    }
    // this.currentItem = this.newsServices.getNewsByID(this.currentId);
  }

  getItemsById(id){
    this.newsServices.getDataById(id).subscribe(res => this.currentItem = res);
  }
}
