import { Component, OnInit } from '@angular/core';
import { BlogService } from '../services/blog.service';

@Component({
  selector: 'app-blog-main',
  templateUrl: './blog-main.component.html',
  styleUrls: ['./blog-main.component.scss']
})
export class BlogMainComponent implements OnInit {

  constructor(private blogService: BlogService) { }
  restList;
  ngOnInit(): void {
  this.blogService.getData().subscribe(res => this.restList = res);
  }
}
