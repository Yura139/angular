import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Blog } from 'src/app/models/blog';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) { }

  private selected: Blog;

  getSelectedNews(): Blog{
    return this.selected;
  }

  setSelectedNews(blog: Blog){
    this.selected = blog;
  }

  getData(): Observable<Blog[]>{
    return this.http.get<Blog[]>('assets/blog.json');
}
}
