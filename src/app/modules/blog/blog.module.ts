import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogMainComponent } from './blog-main/blog-main.component';
import { MaterialModule } from 'src/app/shared/material-module';


@NgModule({
  declarations: [BlogMainComponent],
  imports: [
    CommonModule,
    BlogRoutingModule,
    MaterialModule,
  ]
})
export class BlogModule { }
